import Cookies from 'js-cookie'
import { defineStore } from 'pinia'

// main is the name of the store. It is unique across your application
// and will appear in devtools
export const useMainStore = defineStore('main', {
  // a function that returns a fresh state
  state: () => ({
    lang: 'en',
    locales: {
      en: {},
      vi: {}
    }
  }),
  // optional getters
  getters: {
    //
  },
  // optional actions
  actions: {
    setLanguage(lang: string): void {
      Cookies.set('lang', lang)
      this.lang = lang
    },
    async initialLanguagePack(): Promise<void> {
      //
    }
  }
})
