import type { NuxtConfig } from 'nuxt/schema'

const config: NuxtConfig['mongoose'] = {
  uri: process.env.MONGODB_URI || '',
  options: {
    dbName: 'itsa-home',
    appName: 'itsa-home'
  },
  modelsDir: 'server/models'
}

export default config
