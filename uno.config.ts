// uno.config.ts
import { defineConfig, presetTypography, presetUno, transformerDirectives, transformerVariantGroup, presetAttributify } from 'unocss'
import { presetRemToPx } from '@unocss/preset-rem-to-px'

export default defineConfig({
  shortcuts: [
    // ...
  ],
  theme: {
    colors: {
      style01: '#e74860'
    }
  },
  presets: [
    presetUno(),
    presetRemToPx(),
    presetAttributify(),
    // presetIcons(),
    presetTypography()
    // presetWebFonts({
    //   fonts: {
    //     // ...
    //   }
    // })
  ],
  transformers: [transformerDirectives(), transformerVariantGroup()]
})
