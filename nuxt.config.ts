import AutoImport from 'unplugin-auto-import/vite'
// import mongoConfig from './configs/mongoose.config'
// import tailwindConfig from './tailwind.config'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: {
    // enabled: false,
    enabled: true,
    timeline: {
      enabled: true
    }
  },
  modules: [
    // 'nuxt-mongoose',
    // '@nuxtjs/tailwindcss',
    '@formkit/auto-animate/nuxt',
    '@unocss/nuxt',
    '@element-plus/nuxt',
    '@pinia/nuxt',
    '@nuxtjs/device',
    'nuxt-icons',
    'nuxt-icon',
    '@nuxt/image-edge',
    'nuxt-og-image',
    'nuxt-swiper',
    '@pinia/nuxt',
    '@nuxtjs/i18n',
    '@nuxt/image'
  ],
  i18n: {
    compilation: {
      jit: false
    },
    strategy: 'no_prefix',
    defaultLocale: 'en',
    vueI18n: 'configs/i18n.config.ts', // if you are using custom path, default
    locales: [
      {
        code: 'en',
        iso: 'en-US' // Will be used as "catchall" locale by default
      },
      {
        code: 'vi',
        iso: 'vi-VN'
      }
    ]
  },
  components: [{ path: '~/components/Base', prefix: 'Base' }],
  plugins: [],
  // mongoose: mongoConfig,
  // css: ['~/assets/scss/index.scss'],
  // postcss: {
  //   plugins: {
  //     tailwindcss: {
  //       tailwindConfig
  //     },
  //     autoprefixer: {}
  //   }
  // },
  elementPlus: {
    importStyle: 'scss'
  },
  device: {
    refreshOnResize: true
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@use "@/assets/scss/element-plus/index.scss" as element;`
        }
      }
    },
    build: {
      cssCodeSplit: true,
      minify: true,
      cssMinify: true,
      reportCompressedSize: true,
      rollupOptions: {}
    },
    mode: 'production',
    plugins: [
      AutoImport({
        resolvers: [],
        imports: ['vue', 'vue-router'],
        eslintrc: {
          enabled: true
        },
        dts: './auto-imports.d.ts'
      })
    ]
  },

  // config for production builder
  // ssr: false,
  nitro: {
    preset: 'node-server'
  },
  build: {
    analyze: true
  },

  app: {
    head: {
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1'
    }
  },
  runtimeConfig: {
    // The private keys which are only available within server-side
    apiSecret: '',
    nodeEnv: '',
    // Keys within public, will be also exposed to the client-side
    public: {
      baseApi: '',
      baseUrl: '',
      API_URL: '',
      PUBLIC_KEY: '',
      TINYMCE_KEY: ''
    }
  },
  // mongoose: mongooseConfig,
  ogImage: {
    fonts: [
      // will load this font from Google fonts
      'Source Serif 4:400',
      'Roboto:400',
      'Inter:400',
      'sans-serif:400'
    ]
  },
  swiper: {
    // Swiper options
    // ----------------------
    prefix: 'Swiper',
    styleLang: 'scss'
    // modules: ['navigation', 'pagination'] // all modules are imported by default
  },
  pinia: {
    autoImports: [
      // automatically imports `defineStore`
      'defineStore', // import { defineStore } from 'pinia'
      'storeToRefs',
      ['defineStore', 'definePiniaStore'] // import { defineStore as definePiniaStore } from 'pinia'
    ]
  },
  routeRules: {
    // Homepage pre-rendered at build time
    '/': { prerender: true },
    // Product page generated on-demand, revalidates in background
    '/products/**': { swr: 3600 },
    // Blog post generated on-demand once until next deploy
    '/blog/**': { isr: true },
    // Admin dashboard renders only on client-side
    '/admin/**': { ssr: false },
    // Add cors headers on API routes
    '/api/**': { cors: true },
    // Redirects legacy urls
    '/old-page': { redirect: '/new-page' }
  }
})
