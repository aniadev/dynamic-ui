module.exports = {
  apps: [
    {
      name: 'dynamic-ui',
      script: '.output/server/index.mjs',
      watch: false,
      env: {
        PORT: 4094,
        NODE_ENV: 'development'
      },
      env_production: {
        PORT: 4094,
        NODE_ENV: 'production'
      }
    }
  ]
}
