FROM node:18.18.2-alpine3.17
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json yarn.lock /usr/src/app/
RUN yarn install
RUN yarn build

COPY . /usr/src/app
EXPOSE 4094
CMD [ "yarn", "start" ]
