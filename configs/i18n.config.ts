import enLocale from '@/utils/lang/en.json'
import viLocale from '@/utils/lang/vi.json'

export default defineI18nConfig(() => ({
  legacy: false,
  locale: 'en',
  fallbackLocale: 'en',
  messages: {
    en: {
      ...enLocale,
      welcome: 'Welcome',
      'lang-en': 'English',
      'lang-vi': 'Vietnamese',
      hello: 'hello'
    },
    vi: {
      ...viLocale,
      welcome: 'Xin chèo',
      'lang-en': 'Tiếng Anh',
      'lang-vi': 'Tiếng Việt',
      hello: 'helloooooo'
    }
  }
}))
